// // // fs模块是Node.js的一个核心模块，专门用来操作系统中的文件
// // 在硬盘上写入
// let fs  = require('fs')
// fs.writeFileSync('1.txt','1','utf8')
// let content = fs.readFileSync('1.txt','utf8')
// console.log(content)

// 写入内存
let path = require('path')
var MemoryFileSystem = require('memory-fs');
var fs = new MemoryFileSystem(); // Optionally pass a javascript object

fs.mkdirpSync(path.resolve("dir"));
fs.writeFileSync(path.resolve("dir/file.txt"), "Hello World");
let content2 =fs.readFileSync(path.resolve("dir/file.txt")); // returns Buffer("Hello World")
console.log(content2.toString())
