// 新版可有可无该文件


const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
module.exports = (env, argv) => {
    console.log(env, argv)
    return ({
        // mode:'none',  // 不是生产也不是开发环境，dist文件夹内的文件不压缩，方便学习观看
        // mode: env.production?'production':'development',
        mode: process.env.NODE_ENV,
        devtool: false,
        // optimize:[],
        // entry:'./src/index.js',
        // 等效写法
        entry: {
            main: './src/index.js',  //打包后的文件名默认为main
        },
        output: {
            path: path.resolve(__dirname, 'dist'),// 文件夹
            filename: 'main.js', // 文件名
            // publicPath: '/',// 产出资源的路径前缀
        },
        // 通过express启动一个HTTP服务器，通过它可以访问产出的文件
        // 静态文件根目录是dist
        // 开发环境的配置，譬如webpack-dev-server，打包也生成dist文件写入数据，但是是写入内存中
        // 硬盘上没有，所以看不到
        devServer: {
            // publicPath:'/',
            static: path.resolve('public'),// 额外的静态文件内容的目录
            compress: true,// 压缩
            port: 8080,// 服务器监听的端口号
            open: true,//是否打开浏览器  
        },
        module: { // loader 翻译webpack不认识的文件
            rules: [
                {
                    test: /\.txt$/,
                    use: 'raw-loader',
                },
                {
                    test: /\.css$/i,
                    // 有多个loader可写成如下数组的形式。
                    // 执行顺序从末尾到首元素
                    // 数组第一个元素必须是返回js，因为它的结果是给webpack用
                    use: [
                        "style-loader", // css转成js
                        "css-loader",// url import 进行处理
                        'postcss-loader' // css 预处理，添加厂商前缀
                    ],
                },
            ]
        },
        plugins: [  // 从硬盘输出文件到dist的时候使用plugin
            new HtmlWebpackPlugin({
                template: './src/index.html'
            }),
            new webpack.DefinePlugin({ // 定义可以全局拿到的变量，但是本文件（node环境）拿不到
                // 本质 编译时纯的字符串的替换
                'process.env.NODE_ENV1': JSON.stringify(process.env.NODE_ENV),
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            })
        ]
    })
}